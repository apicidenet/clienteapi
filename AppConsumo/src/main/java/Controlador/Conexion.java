package Controlador;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class Conexion {

    public static String getMethod() {

        StringBuilder informationString = null;

        try {
            URL urlConexion = new URL("http://localhost:8080/empleados/listar");
            HttpURLConnection con = (HttpURLConnection) urlConexion.openConnection();

            con.setRequestMethod("GET");
            con.connect();

            int responseCode = con.getResponseCode();
            if (responseCode != 200) {
                throw new RuntimeException("Ocurrió un error: Código  " + responseCode);
            } else {

                informationString = new StringBuilder();
                Scanner sc = new Scanner(urlConexion.openStream());

                while (sc.hasNext()) {
                    informationString.append(sc.nextLine());
                }

                sc.close();

                System.out.println(informationString);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return informationString.toString();
    }

    public static void postMethod(String cadena) throws IOException {

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

            HttpPost httpPost = new HttpPost("http://localhost:8080/empleados");
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-Type", "application/json");
            String json = cadena;

            StringEntity stringEntity = new StringEntity(json);
            httpPost.setEntity(stringEntity);

            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Ocurrió un error: Código " + status);
                }
            };
            String responseBody = httpclient.execute(httpPost, responseHandler);
            System.out.println("----------------------------------------");
            System.out.println(responseBody);
        }
    }

    public static void putMethod(String cadena, Long id) throws IOException {

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

            HttpPut httpPut = new HttpPut("http://localhost:8080/empleados/" + id);
            httpPut.setHeader("Accept", "application/json");
            httpPut.setHeader("Content-type", "application/json");
            String json = cadena;
            StringEntity stringEntity = new StringEntity(json);
            httpPut.setEntity(stringEntity);

            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Ocurrió un error: Código " + status);
                }
            };
            String responseBody = httpclient.execute(httpPut, responseHandler);
            System.out.println("----------------------------------------");
            System.out.println(responseBody);
        }

    }

    public static void deleteMethod(Long id) throws IOException {

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

            HttpDelete httpDelete = new HttpDelete("http://localhost:8080/empleados/" + id);

            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Ocurrió un error: Código " + status);
                }
            };
            String responseBody = httpclient.execute(httpDelete, responseHandler);
            System.out.println("----------------------------------------");
            System.out.println(responseBody);
        }

    }
}
